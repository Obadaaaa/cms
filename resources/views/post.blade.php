@extends('layouts.blog-post')
@section('content')




 <!-- Title -->
 <h1>{{ $post->title }}</h1>

 <!-- Author -->
 <p class="lead">
     by <a href="#">{{$post->user->name }}</a>
 </p>

 <hr>
 <h2> {{ $post->body }}</h2>

 <!-- Date/Time -->
 <p><span class="glyphicon glyphicon-time"></span>Posted{{ $post->created_at->diffForHumans()}}</p>

 <hr>
 
 <!-- Preview Image -->
 <img class="img-responsive" src="{{ $post->photo->file }}" alt="">

 <hr>

 <!-- Post Content -->

 <hr>
 @if(Session::has('comment_message'))
 {{ session('comment_message') }}
 @endif

@if(Auth::check())

 <div class="well">
     <h4>Leave a Comment:</h4>
   
   
     {{ Form::open(['method'=>'POST','action'=> 'App\Http\Controllers\PostCommentController@store']) }}
     <input type ="hidden" name = "post_id" value="{{ $post->id }}">
     
     <div class ='form-group'>
        {!! Form::label('body','body:') !!}
        {!! Form::textarea('body',null,["class"=>'form-control','rows'=> 3]) !!}
     </div>

<div class="form-group">
    {!! Form::submit('Submit comment',['class'=>'btn btn-primary']) !!}
</div>
{{ Form::close() }}
 </div>
@endif





 <hr>




 <!-- Posted Comments -->


@if(count($comments)>0)

@foreach ($comments as $comment )
    
@if($comment->is_active==1)

 <!-- Comment -->
 <div class="media">
     <a class="pull-left" href="#">
         <img height="64" class="media-object" src="{{$comment->photo}}" alt="">
     </a>
     <div class="media-body">
         <h4 class="media-heading">{{ $comment->author }}
             <small>{{ $comment->created_at->diffForHumans() }}</small>
         </h4>
       <p> {{ $comment->body }}</p>

@foreach ($comment->replies as $reply )
    
@if($reply->is_active==1)
       <div class="nested-comment media">
        <a class="pull-left" href="#">
            <img  height ="64"class="media-object" src="{{ $reply->photo }}" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">{{$reply->author}}
                <small>{{ $reply->created_at->diffForHumans() }}</small>
            </h4>
            <p>{{ $reply->body }}</p>
        
        </div> 

        {{ Form::open(['method'=>"POST","action"=>"App\Http\Controllers\CommentRepliesController@CreateReply"]) }}
        <div class = 'form-group'> 
            <input type ="hidden" name = "comment_id" value="{{$comment->id }}">
        {!! Form::label('body',"Body:") !!}
        {!! Form::textarea('body',null,['class'=>'form-control', 'rows'=>1]) !!}
        </div>
        <div class ='form-group'>
            {!! Form::submit('reply  ',['class'=>'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}

    </div>
     </div>
 </div>
 @endif
 @endforeach
 
 @endif

 @endforeach
 @endif

 <!-- Comment -->
 
         
         <!-- End Nested Comment -->
     </div>
 </div>
@stop