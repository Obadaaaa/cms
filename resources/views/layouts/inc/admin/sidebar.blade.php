<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="index.html">
        <i class="mdi mdi-home menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>




    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
        <i class="mdi mdi-account menu-icon"></i>
        <span class="menu-title">Users</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="auth">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ url('admin/users/create') }}"> Create User </a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ url('admin/users') }}"> All Users</a></li>
         
        </ul>
      </div>
    </li>



    <li class="nav-item">
      <a class="nav-link" href="#posts" data-bs-toggle="collapse" aria-expanded="false" aria-controls="posts">
        <i class="mdi mdi-file-document-box-outline menu-icon"></i>
        <span class="menu-title">Posts</span>
      </a>
      <div class="collapse" id="posts">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ url('admin/posts/create') }}"> Create Post </a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ url('admin/posts') }}"> All Posts</a></li>
         
        </ul>
      </div>
    </li>



    <li class="nav-item">
      <a class="nav-link" href="{{url('admin/categories') }}">
        <i class="mdi mdi-view-headline menu-icon"></i>
        <span class="menu-title">Categories</span>
      </a>
    </li>




  </ul>
</nav>