
@extends('layouts.admin')
@section('content')
@if(Session::has('deleted_user'))
<p class ='bg-danger'> {{session('deleted_user') }}
@endif
<h1>Users</h1>

<div class="container">
  
  <table class="table table-condensed">
    
    <thead>
      <tr>
        
        <th>id</th>
        <th>Photo</th>
        <th>Name</th>
        <th>Email</th>
        <th>Active</th>
        <th>Role </th>
        <th>Created </th>
        <th>Updated </th>
      </tr> 
    </thead>
    <tbody>
     
     
     @if ($user)
     @foreach ( $user as $us  )
     <tr>
        <td>{{ $us->id }}</td>
        <td> <img height="50" src ="/images/{{$us->photo ? $us->photo->file:'no User Photo' }}"></td>
    
        <td><a href="/admin/users/{{$us->id}}/edit" >{{ $us->name}} </a></td>
        <td>{{$us->email }}</td>
        <td>{{$us->is_active == 1?'Active':'No Active'}}</td>
        <td>{{$us->roles['name']}}</td>
        <td>{{$us->created_at->diffForHumans()}}</td>
        <td>{{$us->updated_at->diffForHumans() }}</td> 
      </tr>
     @endforeach
     @endif
    </tbody>
  </table>
</div>


@stop
