@extends('layouts.admin')
@section('content')
<h1> Create User </h1>
{!! Form::model([$user,$roles],['method'=>'PUT','action'=>['App\Http\Controllers\AdminUsersController@update',$user->id],'files'=>true]) !!}
<div class ="form-group">
    <img src="{{ $user->photo? $user->photo->file :'http://placehold.it/400x400'}}" alt="" class="img-responsive img-rounded">
</div>
    <div class ='form-group'>
    
        {!! Form ::label('name','Name') !!}
        {!! Form::text('name',$user->name,['class'=>'form-control']) !!}
    </div>

    <div class ='form-group'>
    
        {!! Form ::label('email','Email') !!}
        {!! Form::text('email',$user->email,['class'=>'form-control']) !!}
    </div>
    <div class ='form-group'>
    
        {!! Form ::label('role_id','Role') !!}
        {!! Form::select('role_id',array('Choose Option'=>$roles),$user->roles['name'],['class'=>'form-control']) !!}
    </div>
    <div class ='form-group'>
    
        {!! Form ::label('status','Status') !!}
        {!! Form::select('is_active',array(1=>'Active',0=>'Not Active'),$user->is_active,['class'=>'form-control']) !!}
    </div>
    <div class ="form-group">
        {!! Form::label('photo_id', 'Photo:') !!} 
        {!! Form::file('photo_id',null,['class'=>'form-control']) !!}
    </div>

    <div class ='form-group'>
    
        {!! Form ::label('password','Password:') !!}
        {!! Form::password('password',['class'=>'form-control']) !!}
    </div>
    <div class ='form-group'>
        {!! Form::submit('Update User',['class'=>'btn btn-primary']) !!} 
    </div>
    

    {!! Form::close() !!}  

{!! Form::open(['method'=>'DELETE' ,'action'=>['App\Http\Controllers\AdminUsersController@destroy',$user->id],])!!}
<div class ='Form Group'>
    {!! Form::submit('Delete User',['class'=>'btn btn-danger']) !!} 
</div>

{!! Form::close() !!}


@stop












<!--
@if(count($errors)>0)

    <ul>
        @foreach ($errors as $err )
        <li> {{ $err }}</li>
        @endforeach
        <ul>
@endif
!-->