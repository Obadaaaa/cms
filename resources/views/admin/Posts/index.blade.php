@extends('layouts.admin');
@section('content')

<h1>Posts</h1>
<!DOCTYPE html> 
<html lang="en">\

<head>
 
</head>
<body>


<div class="container">
  
  <table class="table table-condensed">
    
    <thead>
      <tr>
        
        <th>id</th>
        <th>User</th>
        <th>Photo</th>
        <th>Category</th>
        <th>title</th>
        <th>Body</th>
        
        <th>Created At</th>
        <th> updated at</th>
      </tr> 
    </thead>
    <tbody>
     
     
     @if ($posts)
     @foreach ($posts as $post  )
     <tr>
        <td>{{ $post->id }}</td>
        <td> <a href="/admin/posts/{{$post->id}}/edit">{{$post->user['name'] }}</td>
          @if($post->photo !=null)
        <td> <img  height ='50' src="/images/{{ $post->photo->file }}" alt=""></td>
        @else
        <td> <img  height ='50' src="http://placehold.it/400x400" alt=""></td>
        @endif
        <td>{{$post->category->name }}</td>
        <td> {{$post->title }}</td>
        <td>{{ $post->body }}</td>
 
   
        <td>{{$post->created_at->diffForHumans()}}</td>
        <td>{{$post->updated_at->diffForHumans() }}</td>
        <td><a href="/post/{{ $post->id }}"> View Post </a></td> 
        <td> <a href= "/admin/comments/{{ $post->id }}"> view Comments </a></td>
      </tr>
     @endforeach
     @endif
    </tbody>
  </table>
 
</div>

</body>
</html>

@stop
