@extends('layouts.admin');



@section('content')
<h1> Edit Post </h1>
{!! Form::model([$post,$category],['method'=>'PUT','action'=>['App\Http\Controllers\AdminPostsController@update' , $post->id],'files'=>true]) !!}
<div class ='form-group'>
   
    {!! Form ::label('title','Title:') !!}
    {!! Form::text('title',$post->title,['class'=>'form-control']) !!}
</div>
<div class ='form-group'>
   
    {!! Form ::label('category_id','Category:') !!}
    {!! Form::select('category_id',array([$category]) ,null,['class'=>'form-control']) !!}
</div>

<div class ='form-group'>
   
    {!! Form ::label('photo_id','Photo:') !!}
    {!! Form::file('photo_id',$post->photo->name,['class'=>'form-control']) !!}
</div>

<div class ='form-group'>
   
    {!! Form ::label('body','Description:') !!}
    {!! Form::textarea('body',$post->body,['class'=>'form-control']) !!}
</div>

<div class ='form-group'>
    {!! Form::submit('Edit Post',['class'=>'btn btn-primary']) !!} 
</div>

{!! Form::close() !!}  







{!! Form::open(['method'=>'DELETE','action'=>['App\Http\Controllers\AdminPostsController@destroy' , $post->id],'files'=>true]) !!}

<div class ='form-group'>
    {!! Form::submit('Delete Post',['class'=>'btn btn-danger']) !!} 
</div>

{!! Form::close() !!}  

@stop