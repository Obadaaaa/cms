@extends('layouts.admin')
@section('content')
<h1> Categories</h1>


<div class = 'col-sm-6'>
    {!!Form::open(['method'=>"POST" , 'action'=>'App\Http\Controllers\AdminCategoriesController@store']) !!}
    <div class= 'form-group'>
         {!! Form::label('name','Name') !!}
         {!! Form::text('name',null,['class'=>'form-control']) !!}

    </div>
    <div class= 'form-group'>
     
        {!! Form::submit('create Category',null,['class'=>'btn-priamry']) !!}

   </div>   
        
    {!! Form::close() !!}
</div>

<div class="col-sm-6">
  
  <table class="table table-condensed">
    
    <thead>
      <tr>
        
        <th>id</th>
        <th>type</th>
        <th>Created At</th>
        <th>updated at</th>
      </tr> 
    </thead>
    <tbody>
        
      @if ($category)
     @foreach ($category as $ca  )
     <tr>
<td>{{ $ca->id }}</td>
<td> <a href ="/admin/categories/{{$ca->id}}/edit">{{ $ca->name }} </a></td> 
<td> {{$ca->created_at->diffForHumans() }}
    <td> {{$ca->updated_at->diffForHumans() }}
      </tr>
     @endforeach
     @endif
    </tbody>
  </table>
</div>




@stop
