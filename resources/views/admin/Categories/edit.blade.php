@extends('layouts.admin');



@section('content')
<h1> Edit Category </h1>
{!! Form::model($category,['method'=>'PUT','action'=>['App\Http\Controllers\AdminCategoriesController@update' , $category->id]]) !!}
<div class ='form-group'>
   
    {!! Form ::label('name','Name:') !!}
    {!! Form::text('name',$category->name,['class'=>'form-control']) !!}
</div>
<div class ='form-group'>
    {!! Form::submit('Chnge Category Name',['class'=>'btn btn-primary']) !!} 
</div>

{!! Form::close() !!}  







{!! Form::open(['method'=>'DELETE','action'=>['App\Http\Controllers\AdminCategoriesController@destroy' , $category->id]]) !!}

<div class ='form-group'>
    {!! Form::submit('Delete Post',['class'=>'btn btn-danger']) !!} 
</div>

{!! Form::close() !!}  

@stop