<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'post_id',
        'author',
        'photo',
        'email',
        'body',
        'is_active'
    ];
    public function replies(){
        return $this->hasMany('App\Models\CommentReply','comment_id');
    }
    public function post (){
        return $this->belongsTo('App\Models\Post');
    }
    use HasFactory;
}
