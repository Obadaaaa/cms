<?php

namespace App\Http\Controllers;


use App\Http\Requests\UsersEditRequest;
use App\Http\Requests\UsersRequset;
use App\Models\Photo;
use App\Models\roles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user  = User::all();
        
        
        return view('admin.users.index',compact('user')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = roles::pluck('name','id')->all();

        return view('admin.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequset $request)
    {
        if(trim($request->password == '')){
            $input = $request->except('password');
            
        }
        else{

        
        $input = $request->all();
     $input['password']=bcrypt($request->password);
        if($file = $request->file('photo_id')){

     $name = time() . $file->getClientOriginalName();
     $file->move('images',$name);
     $photo = Photo::create(['file'=>$name]);
     $input['photo_id']=$photo->id;     
          }
      

          User::create($input);
        }
    return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = roles::pluck('name','id')->all();
        return view('admin.users.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersEditRequest $request, $id)
    {
        $user = User::findOrFail($id);
        if(trim($request->password == null)){
          
            $request['password'] = $user->password;
          
        }
        else{

        
        $input = $request->all();
        $input['password']=bcrypt($request->password);}

        $input =$request->all();
     
        if($file = $request->file('photo_id')){

            $name = time() . $file->getClientOriginalName();
            $file->move('images',$name);
            $photo = Photo::create(['file'=>$name]);
            $input['photo_id']=$photo->id;     
                 }
               $user->update($input);
               return redirect('/admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

  $user = User::findOrFail($id);
  
  if ($user->photo != null){
unlink(public_path('images/') . $user->photo->file);}

$user->delete();
  Session::flash('deleted_user','The user has been deleted');
  return redirect('/admin/users');

        
    }
}
